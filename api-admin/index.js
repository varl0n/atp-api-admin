var db_query = require('./api/event-handlers');

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');

    var response;

    if(req.query.get) {

        switch (req.query.get) {
            case 'list-of-organisations': context.res = db_query.getOrganisations(); break;
            default: context.res = {status: 400, body: 'Please provide a valid endpoint.'}
        }
    }

    if (req.query.name || (req.body && req.body.name)) {
        context.res = {
            // status: 200, /* Defaults to 200 */
            body: "Hello " + (req.query.name || req.body.name)
        };
    }
    else {
        context.res = {
            status: 400,
            body: "Please pass a name on the query string or in the request body"
        };
    }
};