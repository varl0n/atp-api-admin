module.exports = {
    database: {
        userName: 'varl0n',
        password: 'Sp2k68s151',
        server: 'atpsqlserver.database.windows.net',
        options:
        {
            database: 'atp-organisations',
            encrypt: true
        }
    },

    devdb: {
        host     : 'localhost',
        port     : '8889',
        user     : 'root',
        password : 'root',
        database : 'atp-db',
    },

    encryption: {
        privateKey: 'A07!}}x~/]Sd%@*'
    }
};