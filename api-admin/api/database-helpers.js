var md5 = require('md5');
var env = require('./env.production');

var db = (function(){
    var Helpers = function(){
    };


    Helpers.prototype.objectPropToSQL = function(object){
        var columns = '';
        var values = '';

        for(var p in object) {
            if(!object.hasOwnProperty(p)) continue;

            columns += ','+p;
            values += ",'"+object[p]+"'";
        }

        columns = columns.substring(1);
        values = values.substring(1);

        return {columns: columns, values:values};
    }


    Helpers.prototype.objectToMD5 = function(object) {
        var str = "";

        for(var p in object) {
            if(!object.hasOwnProperty(p)) continue;
            str += object[p];
        }

        return md5(str+env.encryption.privateKey+(Date.now()));
    }

    return new Helpers;

})();


module.exports = db;