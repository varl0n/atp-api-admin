const express = require('express');
const router = express.Router();
const app = express();
const bodyParser = require('body-parser');
const EventsHandler = require('./event-handlers');

var eve = new EventsHandler();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


router.post('/new-organisation', function(req, resp){ eve.addOrganisation(req, resp, eve.getOrganisations); });
router.post('/list-of-organisations', eve.getOrganisations);
router.post('/delete-organisation', function(req, resp){ eve.deleteOrganisation(req, resp, eve.getOrganisations); });

router.post('*', eve.unknownEndpoint);


module.exports = router;